const http = require('http');
const CI_ID = process.env.CI_ID;
const CI = process.env.CI;

const SERVICE_URL = (!CI) ? 'http://127.0.0.1' : `http://${CI_ID}-service`;
const SERVICE_PORT = '5800';


const getUserRandomNumber = async () => {
    const res = await getRandomNumber();
    return  res.data + 1000;
}

const welcomeMessage = async () => {
    const number = await getUserRandomNumber();
    return `Welcome. You're client number is: ${number}`
}

const getRandomNumber = () => {
    return new Promise((resolve, reject) => {
        http.get(`${SERVICE_URL}:${SERVICE_PORT}`, (res) => {
            res.setEncoding('utf8');
            let rawData = '';
            res.on('data', (chunk) => { rawData += chunk; });
            res.on('end', () => {
                try {
                    const parsedData = JSON.parse(rawData);
                    console.log(parsedData);
                    resolve(parsedData)
                } catch (e) {
                    console.error(`Got error`);
                    reject(e);
                }
            });
        }).on('error', (e) => {
            console.error(`Got error`);
            reject(e);
        });
    });
}

(async () => {
    const message = await welcomeMessage();
    console.log("user message: ",message);
})();


exports.getUserRandomNumber = getUserRandomNumber;
exports.welcomeMessage = welcomeMessage;